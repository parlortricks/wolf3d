(local utils {})


(fn utils.recursive-enumerate [folder file-list]
    (let [items (love.filesystem.getDirectoryItems folder)]
    (each [_ item (ipairs items)]
        (local file (.. folder "/" item))
        (if (love.filesystem.isFile file)
            (table.insert file-list file)
            (love.filesystem.isDirectory file)
            (recursive-enumerate file file-list)))))

(fn require-files [files]
    (each [_ file (ipairs files)]
        (local file (file:sub 1 (- 5)))
        (require file)))

utils