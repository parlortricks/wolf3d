;; global parameters and constants
(local utils (require :src/utils))

(local gfx love.graphics)

;; window properties & initialization
(local CONF {
    :title "Minimal LÖVE"
    :identity :minimal-love
    :width 1280
    :height 720
    :icon (love.image.newImageData "assets/img/fennel-logo.png")
    :flags {:resizable true :vsync true :minwidth 640 :minheight 360}
    :scalex nil
    :scaley nil})


(fn love.load []
    (love.window.setTitle CONF.title)
    (love.filesystem.setIdentity CONF.identity)
    (love.window.setMode CONF.width CONF.height CONF.flags)
    (love.window.setIcon CONF.icon)
)

(fn love.draw []

)

(fn handle-keyboard-input []
    (when (love.keyboard.isDown :escape)
        (love.event.quit)))

(fn love.update [dt]
    (handle-keyboard-input)
)
