# Minimal LÖVE
Minimal LÖVE is the absolute functional minimum required to use Fennel in LÖVE2D and this is also a LowRezJam starter kit

What does the template provide:
 * Maintain/force 64x64 pixel perfect canvas with aspect ratio resizing
 * 3x3 minimal glyph font

## Change Log
 * 2023-4-21:
   * Updated README
   * Updated to Fennel 1.3.0
   * Re-did some of the structure


## Attributions
 * Fennel https://fennel-lang.org/
 * LÖVE2D https://love2d.org/
 * Lowrez Jam Starter Kit for Love2D https://github.com/tcfunk/lowrezjam-starterkit-love2d
 * maid64 https://github.com/adekto/maid64
 * Minimal Fennel Love2D Setup https://gitlab.com/alexjgriffith/min-love2d-fennel
 * Absolutely Minimal Fennel Setup for Love2D https://sr.ht/~benthor/absolutely-minimal-love2d-fennel/
 * Love Fennel https://gitlab.com/alexjgriffith/love-fennel


